# -*- coding: utf-8 -*-
"""
Created on Sat Feb 20 21:04:32 2016

@author: pieter
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as scipysig
import datetime
import os

cwd = os.getcwd()
folder="/W75/"
subfolders=[]

for root, dirs, files in os.walk(cwd+folder, topdown=False):
    for name in dirs:
        subfolders.append(name)
        #print(os.path.join(root, name))


subfolder=subfolders[1]
print(subfolder)
folder_files=[]
for file in os.listdir(cwd+folder+subfolder):
    if file.endswith(".asc"):
        folder_files.append(file)

# Gives file length,
# Stolen verbatim from http://stackoverflow.com/questions/845058/how-to-get-line-count-cheaply-in-python
def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


file= folder_files[4]
print(file)
file = cwd + folder+subfolder+"/"+file
file_importer(file)

  
    

def file_importer(file):
    file_length=file_len(file)
    if file_length != 8204:
        print("Importing WILL (PROBABLY) FAIL. Do not edit the text file before running it through here")
    
    header=np.genfromtxt(file,dtype='str',skip_footer=file_length-6)
    project=header[0,1]
    print(project)
    nch= int(header[1,1]) # Safe to assume it is an integer.
    bw = float(header[2,1]) 
    velocity_header = float(header[3,1])
    pv = float(header[4,1])
    freq = float(header[5,1])
    
    date_time=np.genfromtxt(file,dtype='str',skip_header=6,skip_footer=file_length-8)
    def scope_time(date_time):
        date = datetime.date(int(date_time[0,1]),int(date_time[0,2]),int(date_time[0,3]))
        time = datetime.time(int(date_time[1,1]),int(date_time[1,2]),int(date_time[1,3]))   
        return(date, time)
    
    (date, time) = scope_time(date_time)
    
    
    position=np.genfromtxt(file,dtype='str',skip_header=8,skip_footer=file_length-9)
    
    intergration_times=np.genfromtxt(file,dtype='str',skip_header=9,skip_footer=file_length-10)
    max_v=np.genfromtxt(file,dtype='str',skip_header=10,skip_footer=file_length-11)
    max_value=float(max_v[2])
    x=np.genfromtxt(file,skip_header=11,skip_footer=1)
    x_corr= x * 100 * max_value
    
    
    
    c=299792458.0
    vr = c * bw / freq
    vrb=(vr/nch)/1000
    bin_number=np.linspace(1,nch,nch)
    velocity= -(bin_number - pv*nch)*(vrb/10)
    # NOTE Pythons use of a zero index.
    pol_1=x_corr[0:4096]
    pol_2=x_corr[4096:]
    
    # Error in velocity seems to be constant.
    velocity_error=np.zeros([4096])
    for i in range(0,np.size(velocity)-1):
        #print(velocity(i))    
        velocity_error[i]=abs(velocity[i+1]-velocity[i])/2.0
    print("Error in velocity")
    print(np.amax(velocity_error))
    
    # Plotting Section, Rough Code.
    plt.plot(velocity,pol_1,velocity,pol_2)
    plt.xlabel('Velocity ($km s^{-1}$)')
    plt.ylabel('Normalized Reading')
    plt.title(project + " time:" + str(time))
    
    plt.show()


#for key in folder_files:
#   file = cwd + folder+"/"+key
#   file_importer(file)  

